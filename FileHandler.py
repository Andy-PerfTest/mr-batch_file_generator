import csv
import math
from datetime import datetime
import os
from tkinter import *
from tkinter import filedialog

now = datetime.now()
date_time = now.strftime("%Y%m%d%H%S")

master_file = []


def file_locator():
    root = Tk()

    filename = filedialog.askopenfilename(initialdir="/",
                                          title="Select a File",
                                          filetypes=(("Text files", "*.bat*"),
                                                     ("all files", "*.*")))

    root.withdraw()
    root.update()
    return filename


def master_file_printer():
    write_master_file = open('C:\\Users\\246161\\Desktop\\METER_READS\\batch_file_testing\\6perLP\\MasterFile_SET2_' + date_time + '.bat', 'w')
    write_master_file.writelines(master_file)

    write_master_file.seek(0, os.SEEK_END)
    write_master_file.seek(write_master_file.tell() - 2, os.SEEK_SET)
    write_master_file.truncate()
    write_master_file.close()

    pass


class FileHandler:
    def __init__(self):
        self.dir_path = file_locator() # 'S:\\TSB-TB\\Application-Control-Book\\QAT\\PERFORMANCE TESTING\\Wave7\\02 - Test Data\\Meter Details for 3.9M\\Batch2-2M-5.27.20212021.05.27-23.25.49\\Perf_A173_set2_LP1.bat'  # file_locator()
        # self.dir_path_lP3 = file_locator()
        self.lp_data = 0
        self.batch_file_list = []
        self.records_per_file = 0
        self.number_of_files = 0
        self.file_name = ''
        self.executions = 6

    def read_file(self, lp_data):
        self.lp_data = lp_data
        with open(self.dir_path, 'r', newline='', ) as sample_data:
            batch_file_reader = csv.reader(sample_data, delimiter='\n', quoting=csv.QUOTE_NONE)
            for line in batch_file_reader:
                self.batch_file_list.append(line)

    def breaker(self):
        self.number_of_files = len(self.batch_file_list)
        self.records_per_file = math.ceil(self.number_of_files / self.executions)
        return self.records_per_file, self.number_of_files

    def create_files(self):
        final = [self.batch_file_list[i * self.records_per_file:(i + 1) * self.records_per_file] for i in range((self.number_of_files + self.records_per_file - 1) // self.records_per_file)]
        print(final)

        for line in range(0, self.executions):
            csv.register_dialect('print')
            self.file_name = 'Perf-SET2-LP' + str(self.lp_data) + '-' + date_time + '_' + str(line + 1) + '.bat'
            execute_batch_file = 'start cmd /c ' + self.file_name + "\n"
            master_file.append(execute_batch_file)

            filename = 'C:\\Users\\246161\\Desktop\\METER_READS\\batch_file_testing\\6perLP\\' + self.file_name
            with open(filename, 'w', encoding='utf8', newline='') as csvfile:
                writer = csv.writer(csvfile, delimiter=',', escapechar="\n", quotechar=None, quoting=csv.QUOTE_NONE)
                logging = csv.writer(csvfile, delimiter=',', escapechar="\n", quotechar=None, quoting=csv.QUOTE_NONE)

                logging.writerow(['cd D:\\UtilityIQ\\tibco-tools'])
                logging.writerow(['d:'])
                logging.writerow(['echo Start time %date% %time% > SET2_test_LP' + str(self.lp_data) + '_' + str(line + 1) + '.log'])
                writer.writerows(final[line])  # prints all the lines per page
                logging.writerow(['echo End Time %date% %time% >> SET2_test_LP' + str(self.lp_data) + '_' + str(line + 1) + '.log'])

                # removes blank/empty line
                csvfile.seek(0, os.SEEK_END)
                csvfile.seek(csvfile.tell() - 2, os.SEEK_SET)
                csvfile.truncate()
        pass


sample_lp1 = FileHandler()
sample_lp1.read_file(1)
sample_lp1.breaker()
sample_lp1.create_files()
del sample_lp1

sample_lp3 = FileHandler()
sample_lp3.read_file(3)
sample_lp3.breaker()
sample_lp3.create_files()

master_file_printer()
